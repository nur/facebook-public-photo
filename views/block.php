<?php for ($i = 0;$i<$col;$i++): ?>                                
    <div>
        <?php for ($j = 0;$j<$row;$j++): ?>
        <?php $index = $i*$row+$j; ?>
        <?php if(isset($urls[$index])):?>
        <?php $url = $urls[$index];?>    
        <div style="padding:3px 6px;">
           <a popup="<?php echo $url['src'];?>" title="<?php echo $url['title'];?>" class="binnash-popup" rel="#binnash-popup-target"  >
                <div class="image" style="<?php echo $url['style'];?>"></div>
            </a>
        </div>
        <?php endif;?>
        <?php endfor;?>
    </div>
<?php endfor;?>
