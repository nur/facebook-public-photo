<link rel='stylesheet' href='<?php echo $this->plugin_url?>/css/facebook-public-photo-horizontal.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo $this->plugin_url?>/css/facebook-public-photo-<?php echo $attrs['theme'];?>.css' type='text/css' media='all' />
<script type="text/javascript">
binnash_facebook_public_photo_<?php echo $uid;?>_var = <?php echo json_encode($links);?>
</script>
<div class="binnash-gridbox-horizontal binnash-gridbox-<?php echo $attrs['theme'];?>" >
    <div id="binnash_facebook_public_photo_<?php echo $uid; ?>"  class="binnash-container">
        <div class="binnash-screen">
            <?php if($title):?>
            <h3 class="binnash-title binnash-title-position"><?php echo $title;?></h3>
            <?php endif;?>
            <div id="binnash-photo-album-" class="binnash-photo-album">         
                <table>
                    <tr>
	                    <td>
                            <div class="binnash-prev-container">
                            <div style="height:10px;"></div>
                            <div class="binnash-prev"></div>
                            </div>
                        </td>
	                    <td>
                            <div class="stripmask">
                                <div class="strip">
                                    <?php for ($k = 0; $k<$num_blocks;$k++):?>                         
                                    <?php $this->generate_block($row,$col,array_slice($urls,$k*$row*$col, $row*$col));?>
                                    <?php endfor;?>
                                </div>
                            </div>
                        </td>
	                    <td >        
                            <div class="binnash-next-container">
                            <div class="binnash-next"></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="binnash-apple-overlay" id="binnash-popup-target">
            <div id="binnash-popup-left" ><div></div></div>
            <div id="binnash-popup-center">
            <img class="binnash-poster"  src="" />
            <div class="details">
                <?php if($title):?>
                <h2><?php echo $title;?></h2>
                <?php endif;?>
                <p></p>
            </div>
            </div>
            <div id="binnash-popup-right" ><div></div></div>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($){
    var config = <?php echo json_encode($config);?>;
        config.orientation = 'horizontal';
    $('#binnash_facebook_public_photo_<?php echo $uid; ?>').BinnashImageSlider(config);
});
</script>

