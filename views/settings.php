<div class="wrap">
<?php if(!empty($this->message)):?>
<?php echo $this->message; ?>
<?php endif; ?>
	<div id="poststuff">
	<div id="post-body">							
		<form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">	    	    
		<div class="postbox">
    		<h3><label for="title">General Settings</label></h3>    		
			<div class="inside">
			    <table width="100%" border="0" cellspacing="0" cellpadding="6">
			    	<tbody>
			    	<tr valign="top">
					    <td width="25%" align="left">
					    	<strong>Album URL</strong>
				    	</td>
				    	<td align="left">
				    	<input type="text" id="setting-album-url" name="setting[album_url]" size="100" value="<?php echo stripslashes(isset($setting['album_url'])?$setting['album_url']:"");?>">
			    		<p class="description">Go To Your Facebook Album page that is publicly shared.Scroll down to the bottom of the page where you'll Something like the following:<img src="<?php echo $this->plugin_url;?>/images/facebook-link.png" />
                        <br/>Copy the link address and paste in the text field above.</p>
				    	</td>
				    </tr>
			    	<tr valign="top">
					    <td width="25%" align="left">
					    	<strong>Custom Title</strong>
				    	</td>
				    	<td align="left">				    	
				    	<input type="text" id="setting-title-text" name="setting[title_text]" size="100" value="<?php echo stripslashes(isset($setting['title_text'])?$setting['title_text']:"");?>">
			    		<p class="description">Setting Title Here will overwrite title set in facebook. If you want to show facebook title then keep this field empty.<br/> You can disable Title completely using following checkbox.
<input type="checkbox" id="setting-disable-title" name="setting[disable_title]" size="100" value="1"></p>
				    	</td>
				    </tr>
			    	<tr valign="top">
					    <td width="25%" align="left">
					    	<strong>Enable Autoscroll</strong>
				    	</td>
				    	<td align="left">
				    	<input type="checkbox" id="setting-enable-autoscroll" name="setting[enable_autoscroll]" checked="checked" size="100" value="1">
			    		<p class="description">This will allow the images to scroll automatically.</p>
				    	</td>
				    </tr>
			    	<tr valign="top">
					    <td width="25%" align="left">
					    	<strong>Slider Dimension</strong>
				    	</td>
				    	<td align="left">
				    	<select id="setting-dimension" name="setting[dimension]" value="<?php echo stripslashes(isset($setting['dimension'])?$setting['dimension']:"");?>">
                            <option value="h_1_1">1x1(horizontal)</option>                         
                            <option value="h_1_2">1x2(horizontal)</option>                         
                            <option value="h_2_2">2x2(horizontal)</option>                                              
                            <option value="h_1_3" selected="selected">1x3(horizontal)</option>                
                            <option value="h_2_3">2x3(horizontal)</option>                         
                            <option value="h_3_3">3x3(horizontal)</option>
                            <option value="v_1_1">1x1(vertical)</option>       
                            <option value="v_2_1">2x1(vertical)</option>       
                            <option value="v_2_2">2x2(vertical)</option>       
                            <option value="v_3_1">3x1(vertical)</option>       
                            <option value="v_3_2">3x2(vertical)</option>       
                            <option value="v_3_3">3x3(vertical)</option>                         
                        </select>
			    		<p class="description">Choose how many photos you want row and column wise in each slide.You can choose scrolling direction (vertical/horizontal) at the same time.</p>
				    	</td>
				    </tr>
                    <tr>
					    <td width="25%" align="left">
					    	<strong>Theme</strong>
				    	</td>
				    	<td align="left">
                            <select id="setting-theme" name="setting[theme]" >
                                <option selected="selected" value="white">White</option>
                                <option value="black">Black</option>
                                <option value="red">Red</option>
                                <option value="brick">Brick Red</option>
                                <option value="magenta">Magenta</option>
                                <option value="transparent">Transparent</option>
                            </select>
                            <p class="description">You can set different themes for that slider</p>
                        </td>
                    </tr>
                    <tr>
					    <td width="25%" align="left">
					    	<strong>Transition Effect</strong>
				    	</td>
				    	<td align="left">
                            <select id="setting-effect" name="setting[effect]" >
                                <option value="linear">linear</option>               
                                <option value="swing">swing</option>
                                <option value="jswing" selected="selected">jswing</option>
                                <option value="easeInQuad">easeInQuad</option>
                                <option value="easeInCubic">easeInCubic</option>
                                <option value="easeInQuart">easeInQuart</option>
                                <option value="easeInQuint">easeInQuint</option>
                                <option value="easeInSine">easeInSine</option>
                                <option value="easeInExpo">easeInExpo</option>
                                <option value="easeInCirc">easeInCirc</option>
                                <option value="easeInElastic">easeInElastic</option>
                                <option value="easeInBack">easeInBack</option>
                                <option value="easeInBounce">easeInBounce</option>
                                <option value="easeOutQuad">easeOutQuad</option>
                                <option value="easeOutCubic">easeOutCubic</option>
                                <option value="easeOutQuart">easeOutQuart</option>
                                <option value="easeOutQuint">easeOutQuint</option>
                                <option value="easeOutSine">easeOutSine</option>
                                <option value="easeOutExpo">easeOutExpo</option>
                                <option value="easeOutCirc">easeOutCirc</option>
                                <option value="easeOutElastic">easeOutElastic</option>
                                <option value="easeOutBack">easeOutBack</option>
                                <option value="easeOutBounce">easeOutBounce</option>
                                <option value="easeInOutQuad">easeInOutQuad</option>
                                <option value="easeInOutCubic">easeInOutCubic</option>
                                <option value="easeInOutQuart">easeInOutQuart</option>
                                <option value="easeInOutQuint">easeInOutQuint</option>
                                <option value="easeInOutSine">easeInOutSine</option>
                                <option value="easeInOutExpo">easeInOutExpo</option>
                                <option value="easeInOutCirc">easeInOutCirc</option>
                                <option value="easeInOutElastic">easeInOutElastic</option>
                                <option value="easeInOutBack">easeInOutBack</option>
                                <option value="easeInOutBounce">easeInOutBounce</option>
                            </select>
                            <p class="description">Add animation to your slider.</p>
                        </td>
                    </tr>
                    <tr>
					    <td width="25%" align="left">
					    	<strong>Transition Interval</strong>
				    	</td>
				    	<td align="left">
                            <select id="setting-effect" name="setting[delay]" >
                                <option value="0.5">0.5 Second</option>               
                                <option value="1">1 Second</option>
                                <option value="2">2 seconds</option>
                                <option value="3">3 Seconds</option>
                                <option value="4">4 Seconds</option>
                                <option value="5" selected="selected">5 Seconds</option>
                                <option value="6">6 Second</option>
                                <option value="7">7 seconds</option>
                                <option value="8">8 Seconds</option>
                                <option value="9">9 Seconds</option>
                                <option value="10">10 Seconds</option>

                            </select>    
                            <p class="description">Select delay in number of second between transitions.</p>
                        </td>
                    </tr>
			    	<tr valign="top">
					    <td width="25%" align="left">
					    	<strong>Generated Shortcode</strong>
				    	</td>
				    	<td align="left">
				    	<input type="text" id="setting-shortcode" readonly="readonly" name="setting[shortcode]" size="100" value="<?php echo stripslashes($setting['shortcode']);?>" onclick="this.select();" onfocus="this.select();">
			    		<p class="description">When You press The "Generate Shortcode " Button, The shortcode will appear in the text field above.</p>
				    	</td>
				    </tr>
				   </tbody></table>	
			</div>	    	    
			<div class="submit" style="margin:15px;">
				<input type="submit" class="button-primary" id="<?php echo $this->plugin_id;?>_setting_submit" name="<?php echo $this->plugin_id;?>_setting_submit" value="Generate Shortcode">  
			</div>			
		</div>
	    </form>	    
	</div>
	</div>
	<div id="poststuff">
	<div id="post-body">							
		<form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">	    	    
		<div class="postbox">
    		<h3><label for="title">Reset Cache</label></h3>    		
			<div class="inside">
			<div class="submit" style="margin:15px;">
				<input type="submit" class="button-primary" id="reset_cache" name="reset_cache" value="Reset Cache">  
                <p class="description">If the photo album in facebook is updated, You should consider resetting cache..</p>
			</div>			
			</div>			
		</div>
	    </form>	    
	</div>
	</div>
</div>
