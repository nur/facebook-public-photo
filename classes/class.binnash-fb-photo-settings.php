<?php
class BinnashFbPhotoSettings{
    protected static $_this;
    public $album_tbl;   
    public $pages; 
    public $plugin_name = "Facebook Photo";
    public $plugin_slug = "facebook-photo.php";
    public $menu_type   = "option_page";
    Public $default_page= "setting_page";
    public $version     = FACEBOOKPHOTO_VER;     
    protected function __construct(){
        $this->pages     = json_decode(json_encode(array(
            'setting_page'=>array('title'=>"Facebook Photo")
        )));
    }
    public static function get_instance(){
        $class = get_called_class();
        self::$_this = empty(self::$_this)? new $class():self::$_this;
        return self::$_this; 
    }
}
