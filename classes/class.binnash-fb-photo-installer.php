<?php
class BinnashFbPhotoInstaller{
    public static function activate(){
        $settings = BinnashFbPhotoFactory::get_singleton('binnash-fb-photo-settings'); 
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $sql = "CREATE TABLE " . $settings->album_tbl . " (
        id INT(12) NOT NULL PRIMARY KEY AUTO_INCREMENT,
        title VARCHAR(255) NOT NULL,
        shortcode VARCHAR(255) NOT NULL,
        album_ids text,
        meta text,
        fan_page_id INT(12) DEFAULT 0,
        excluded_album_ids text,
        excluded_photo_ids text, 
        type TINYINT DEFAULT 0
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";

        dbDelta($sql);
    
    }
    public static function deactivate(){
    
    }
}
