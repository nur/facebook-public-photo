<?php
class BinnashFbPhotoFactory{
    private function __construct(){}
    private static function load($class){
        $file = 'class.'. $class;
        $path = '';
        if (defined('FACEBOOKPHOTO_PRO')){
            $path .= 'pro/';
            $file .= '-pro';
            $class.= '-pro';
        }                
        $file .= '.php'; 
        $class = str_replace(' ', '', ucwords(str_replace('-', ' ', $class)));
        if(!class_exists($class))include_once($path.$file);
        return $class;
    } 
    public static function get_instance($class){
        $class = self::load($class);
        return new $class();
    }
    public static function get_singleton($class){
        $class = self::load($class);
        return $class::get_instance();
    }
}
