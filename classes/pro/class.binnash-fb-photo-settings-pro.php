<?php
include_once(BFACEBOOKPHOTO_PATH.'classes/class.binnash-fb-photo-settings.php');
class BinnashFbPhotoSettingsPro extends BinnashFbPhotoSettings{
    public $menu_type    = "menu_page";
    Public $default_page = "manage_page";
    protected function __construct(){
        global $wpdb;
        $this->album_tbl = $wpdb->prefix.'binnash_facebook_albums';
        $this->pages     = json_decode(json_encode(array(
            'manage_page'=>array('title'=>"Facebook Photo")
        )));
    }    
} 
