<?php
class BinnashFbPhotoWidget extends WP_Widget {
	function __construct($args = array()) {
		parent::__construct(
	 		'FbPublicPhotoSliderWidget',
			'Facebook Public Photo Slider',
			array( 'description' =>'Add Slider on the sidebar' )
		);
	}
	function widget($args, $instance) {
		extract( $args );
		$shortcode = $instance['binnash_sidebar_Shortcode'] ;
        $title = '';
		echo $before_widget;
		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;
        echo do_shortcode($shortcode);
		echo $after_widget;
	}
	function update($new_instance, $old_instance) {
    	$instance = array();
		$instance['binnash_sidebar_Shortcode'] = strip_tags( $new_instance['binnash_sidebar_Shortcode'] );
		return $instance;
	}
	function form($instance) {
        $shortcode = "";
		if ( isset( $instance[ 'binnash_sidebar_Shortcode' ] ) ) {
			$shortcode = $instance[ 'binnash_sidebar_Shortcode' ];
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'binnash_sidebar_Shortcode' ); ?>"><?php _e( 'Shortcode:' ); ?></label> 
		<textarea class="widefat" id="<?php echo $this->get_field_id( 'binnash_sidebar_Shortcode' ); ?>" name="<?php echo $this->get_field_name( 'binnash_sidebar_Shortcode' ); ?>"   ><?php echo esc_attr( $shortcode ); ?></textarea>
		</p>
        <?php
	}
}
