<?php
include_once ('class.binnash-fb-photo-base.php');
include_once ('class.binnash-fb-photo-widget.php');

class  BinnashFbPhoto extends BinnashFbPhotoBase{
	var $admin_js = array('jquery');
    var $front_js = array('jquery','facebook-public-photo','jquery.easing-1.3.min','jquery.tools.min');
    function __construct(){    
        parent::__construct(); 
        add_action( 'widgets_init', create_function( '', 'register_widget( "BinnashFbPhotoWidget" );' ) );  
        add_filter('widget_text', 'do_shortcode',11);
    }
    function setting_page(){	
        if(isset($_POST['reset_cache'])){
            if(isset($this->options['cache']))
                $this->set_options(array('cache'=>array()));
        }
	    else if(isset($_POST[$this->plugin_id.'_setting_submit'])){
	        $this->set_options(array('setting'=>$_POST['setting']));
            $attrs = $_POST['setting']; unset($attrs['shortcode']);          
            $shortcode = '[binnash_facebook_photo ' . self::array_to_string($attrs, "  ") . ' ]';
            $this->options['setting']['shortcode'] = $shortcode;
	        $this->message = '<div id="message" class="updated fade"><p>Shortcode Generated!</p></div>';
	    }
        else{
            $this->options['setting']['shortcode'] = '';
        }
    	$setting = $this->options['setting'];
        
    	include_once($this->plugin_path.'/views/settings.php');
    }
    function binnash_facebook_photo__shortcode($attrs){ 
        if(!isset($attrs['album_url'])||empty($attrs['album_url'])) return;
        $hash = md5($attrs['album_url']);
        if(isset($this->options['cache'])&&isset($this->options['cache'][$hash])){ 
            $cache = $this->options['cache'][$hash];
            $title = $cache['title'];
            $urls  = $cache['urls']; 
            $links = $cache['links'];
            $link_count = count($cache['links']);
        }
        else{
            $response = self::get_url_contents(html_entity_decode($attrs['album_url']));
        
            $parsed_data = $this->parse_response($response);
            $title = $parsed_data['title'];
            $urls  = $parsed_data['urls'];
            $links = $parsed_data['links'];
            $link_count = count($parsed_data['links']);
        }
        if(!empty($urls)){
            $defaults = array('effect'=>'linear','effect'=>'white','delay'=>5);  
            $attrs = self::sanitize_config($attrs, $defaults);
            if(isset($attrs['disable_title']))
                $title = false;        
            else
                $title = (!isset($attrs['title_text'])||empty($attrs['title_text']))? $title : $attrs['title_text'];            
            $config = array('effect'=>$attrs['effect'],
                            'autoscroll'=>isset($attrs['enable_autoscroll']));  
            $total_images = count($urls);
            ob_start();          
            $dimension = explode('_',$attrs['dimension']);
            if(count($dimension) ==3){
                $orientation = $dimension[0];
                $row = $dimension[1];
                $col = $dimension[2];
                if($row>3 || $col>3){
                    $error = 'Selected row/column combination is not supported.';
                    include_once ($this->plugin_path.'/views/error.php');
                }
                if($orientation == 'h'){
                    $uid = uniqid();
                    $num_blocks = ceil($total_images/ ($row*$col)); 
                    $config['num_blocks'] = $num_blocks;  
                    $config['mask_height'] = 116*$row;
                    $config['mask_width'] = 172*$col;
                    $config['link_count'] = $link_count;
                    include_once($this->plugin_path.'/views/hslider.php');                    
                }
                else if ($orientation == 'v'){
                    $uid = uniqid();
                    $num_blocks = ceil($total_images/ ($row*$col));
                    $config['num_blocks'] = $num_blocks;  
                    $config['mask_height'] = 116*$row;
                    $config['mask_width'] = 172*$col;
                    $config['link_count'] = $link_count;
                    include_once($this->plugin_path.'/views/vslider.php');                
                }
                else{
                    $error = 'Dimension Parameters are not correct';
                    include_once ($this->plugin_path.'/views/error.php');
                }
            }
            else{
                $error = 'Dimension Parameters are not correct';
                include_once ($this->plugin_path.'/views/error.php');    
            }
            $out = ob_get_contents();  
            ob_end_clean();
            return $out;            
        }
        else{
            ob_start();
            $error = get_transient('binnash-facebook-photo');
            if(empty($error))
            $error = 'Unable to fetch album.Make sure the album is publicly shared.';
            include_once ($this->plugin_path.'/views/error.php');
            $out = ob_get_contents();  
            ob_end_clean();
            return $out;            
        }
    }
    function parse_response($response){
        if(empty($response)) return array('title'=>'','urls'=>'','links'=>'');
        $response = json_decode($response);
        $data = $response->data;
        $urls = array();
        $title = '';
        $link_count  = 0;
        $links = array();        
 
        foreach($data as $obj){
            $style = "background-image: url(".$obj->picture.");";                        
            $links['link_'.$link_count] = $obj->source;
            $urls[] = array('title'=>isset($obj->name)?$obj->name: "",'style'=>$style,'src'=>$link_count);
            $link_count++;       
        }  
        $cache = isset($this->options['cache'])? $this->options['cache'] : array();
        $link_info = array('title'=>$title,'urls'=>$urls,'links'=>$links);
        $this->set_options(array('cache'=>$cache));
        return $link_info;
    }
    function generate_block($row, $col, $urls){;
        include($this->plugin_path.'/views/block.php');
    }
    static function get_page_list(){
        $pages = array('setting_page'=>array('title'=>"Facebook Photo"));
        return json_decode(json_encode($pages));
    }
}

