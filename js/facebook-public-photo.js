;(function($){
    var ImageSlider = function(){    
        var defaults = {'effect':'linear','autoscroll':false,'orientation':'horizontal','delay':5};
        var slide = function (strip, dir,parent){
            var options = parent.data('options');
            if(options.on_focus || options.popup_loaded) return;
            if(dir =='l')
                options.current_position++; 
            else if(dir == 'r')
                options.current_position--;
            if(options.current_position<0) options.current_position = options.block_count-1;
            else options.current_position = (options.current_position%options.block_count); 
            if(options.orientation == 'horizontal'){ 
                strip.animate({'left': -(options.current_position*options.step_size)+'px'},1000,options.effect);}
            else if(options.orientation == 'vertical'){
                strip.animate({'top': -(options.current_position*options.step_size)+'px'},1000,options.effect);}
            parent.data('options',options);
        };
        return {
            init: function(opts){                
                return this.each(function(){ 
                    var options = $.extend({}, defaults, opts),
                    binnash_container  = $(this),mask,
                    binnash_next   = $('.binnash-next',binnash_container),
                    binnash_prev   = $('.binnash-prev',binnash_container),
                    stripribbon =$('.strip',binnash_container);
                    options.block_count = options.num_blocks;
                    //horizontal
                    if(options.orientation =='horizontal'){
                        mask=$('.stripmask',binnash_container).css({'width':(options.mask_width)+'px',
                                                      'height':(options.mask_height)+'px'});                    
                        options.step_size = options.mask_width; 
                        binnash_container.css('width',(options.mask_width+96)+'px');
                        options.ribbonlength = options.mask_width*options.num_blocks; 
                        stripribbon.css('width',options.ribbonlength+'px');
                        var button_height = options.mask_height/2+40;
                        $('.binnash-prev-container', binnash_container).css('height',button_height+'px');
                        $('.binnash-next-container', binnash_container).css('height',button_height+'px');
                    }
                    else if(options.orientation='vertical'){
                        mask = $('.stripmask',binnash_container).css({'width':(options.mask_width)+'px',
                                                      'height':(options.mask_height)+'px'});
                        options.step_size = options.mask_height;
                        binnash_container.css('height',(options.mask_height+170)+'px');
                        options.ribbonlength = options.mask_height*options.num_blocks;
                        stripribbon.css('height',options.ribbonlength+'px');
                    }
                    options.block_count = Math.ceil(options.ribbonlength/options.step_size);
                    options.current_position = 0;
                    options.on_focus = false;
                    options.popup_loaded = false;
                    binnash_container.data('options',options);
                    binnash_next.bind('click',function(e){
                        slide(stripribbon,'r',binnash_container);
                    });
                    binnash_prev.bind('click',function(e){
                        slide(stripribbon, 'l',binnash_container);
                    });                    
                    mask.hover(function(e){
                        options.on_focus = true;                        
                    },function(e){
                        options.on_focus = false;
                    });
                    if(options.autoscroll)setInterval(function(){ slide(stripribbon, 'l',binnash_container);},options.delay*1000);
                    var target = $('#binnash-popup-target', binnash_container);  
                    /*
                    $('.strip a[rel]', binnash_container).click(function(){                        
                        var canvas = target.find('img.binnash-poster');
                        var url    = $(this).attr('popup');
                        var links = window[binnash_container.attr('id')+'_var'];
                        canvas.attr('src', links['link_'+url]); 
                        target.overlay().load();
                        
                    });
                    target.overlay({effect: 'apple' });*/    
                    $('.strip a[rel]', binnash_container).overlay({
                        effect:'apple',
                        target:target,
                        onBeforeLoad:function(){
                            var links = window[binnash_container.attr('id')+'_var'];
                            var num_links = options.link_count;
                            var overlay = this.getOverlay();
                            var trigger = this.getTrigger();
                            var url     = trigger.attr('popup');
                            var title   = trigger.attr('title');
                            var left    = $('#binnash-popup-left',overlay);
                            var right   = $('#binnash-popup-right',overlay);
                            var canvas  = overlay.find('img.binnash-poster');
                            left.bind('click', function(){ 
                                if(url>0){url--;canvas.attr('src',links['link_'+url]);}
                            });
                            left.bind('keydown',function(e){
                                if(e.which == 37){
                                    if(url>0){url--;canvas.attr('src',links['link_'+url]);}
                                }
                            });
                            right.bind('click',function(e){
                                if(url<num_links){url++;canvas.attr('src',links['link_'+url]);}
                            });
                            right.bind('keydown',function(e){
                                if(e.which == 39){
                                    if(url<num_links){url++;canvas.attr('src',links['link_'+url]);}
                                }
                            });
                            canvas.attr('src',links['link_'+url]);
                            overlay.find('p').html(title);
                            overlay.find('#preloader').show();
                        },
                        onLoad:function(){
                            options.popup_loaded = true;
                            this.getOverlay().children('#preloader').hide();
                        },
                        onClose:function(){            
                            options.popup_loaded = false;
                            var overlay = this.getOverlay();
                            $('#binnash-popup-left',overlay).unbind('click');
                            $('#binnash-popup-right',overlay).unbind('click');
                            //this.getOverlay().find('img').attr('src',"");            
                        }
                    });        
                    
                });
            }
        };    
    }();
    $.fn.extend({
        BinnashImageSlider : ImageSlider.init
    });
})(jQuery);
