<?php
/*
Plugin Name: Facebook Public Photo
Version: v1.0.2
Plugin URI: http://www.binnash.com/facebook-public-photo-slider
Author: Binnash
Author URI: http://www.binnash.com
Description: This Wordpress Plugin Enables User To Show Publicly Shared Photo Albums On Wordpress Site.
*/
include_once('classes/class.facebook-photo-factory.php');
define('FACEBOOKPHOTO_VER', '1.0.2');
define('BFACEBOOKPHOTO_PATH', dirname(__FILE__) . '/');
define('BFACEBOOKPHOTO_URL', plugins_url('',__FILE__));
define('BFACEBOOKPHOTO_BASE', __FILE__);
define('FACEBOOKPHOTO_PRO', true);
register_activation_hook( __FILE__,  'binnash_fb_photo_install');
register_deactivation_hook( __FILE__, 'binnash_fb_photo_uninstall');

add_action('plugins_loaded', 'load_wp_facebook_photo_object');
function load_wp_facebook_photo_object(){	
    BinnashFbPhotoFactory::get_instance('binnash-fb-photo');
}
function binnash_fb_photo_install(){
    $class = BinnashFbPhotoFactory::load('binnash-fb-photo-installer');
    $class::activate();
}
function binnash_fb_photo_uninstall(){
    $class = BinnashFbPhotoFactory::load('binnash-fb-photo-installer');
    $class::deactivate();
}

